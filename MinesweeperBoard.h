//
// Created by x240 on 18.03.2022.
//

#ifndef SAPER_MINESWEEPERBOARD_H
#define SAPER_MINESWEEPERBOARD_H
enum GameMode  { DEBUG, EASY, NORMAL, HARD };
enum GameState { RUNNING, FINISHED_WIN, FINISHED_LOSS };

struct Field
{
    bool hasMine;
    bool hasFlag;
    bool isRevealed;
    //bool isPlayable;
};


class MinesesweeperBoard
{
    Field board[100][100];
    int width;
    int height;
    int Mines;
    bool firstMove;
    GameState state;
    void clear_board();
    bool isPlayable(int col, int row)const;

public:

    MinesesweeperBoard( GameMode mode);
    void debug_display( )const;
    bool has_mine(int col, int row)const;
    int getBoardWidth() const;
    int getBoardHeight() const;
    int getMineCount() const;
    void setMines();
    bool victory();
    int countMines(int col, int row) const;

    bool has_Flag(int col, int row) const;

    void toggleFlag(int col, int row);

    void revealField(int col, int row);

    bool isRevealed(int col, int row) const;

    GameState getGameState() ;

    char getFieldInfo(int col, int row) const;
};




#endif //SAPER_MINESWEEPERBOARD_H
