#include <iostream>

#include <string>

#include <cstdlib>

#include "MinesweeperBoard.h"
#include "MinesweeperBoardTextView.h"
#include "MSTextController.h"
#include "MSSFMLView.h"
#include <SFML/Graphics.hpp>

using namespace std;

void intro(){
    cout<<"Michal, 98"<<endl;
}


int main()
{
    srand(time(nullptr));


    intro();

    MinesesweeperBoard plansza (EASY);//plansza
//plansza.getBoardWidth();
   // plansza.debug_display();

    MSBoardTextView view (plansza);
    MSTextController gra(view,plansza);
    //view.display();

    plansza.debug_display();
   // plansza.getBoardHeight();
   //gra.play();


    sf::RenderWindow window(sf::VideoMode(800, 600), "Grafika w C++/SFML");
    window.setVerticalSyncEnabled(false);
    window.setFramerateLimit(30);


    MinesesweeperBoard board( EASY);
    MSSFMLView View (board);  // przekazujemy przez referencję planszę jako argument konstruktora

    // symulujemy rozgrywkę
    board.toggleFlag(0,0);
    board.revealField(2,3);
    board.revealField(0,1);
    board.revealField(1,2);
    board.revealField(3,1);
    board.debug_display();
    while (window.isOpen())
    {
        sf::Event event;
        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed)
                window.close();
        }

        window.clear();
        View.draw(window);

        window.display();
    }

 return 0;
}