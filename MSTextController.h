//
// Created by gracz on 2022-04-07.
//

#ifndef SAPER2_MSTEXTCONTROLLER_H
#define SAPER2_MSTEXTCONTROLLER_H
#include "MinesweeperBoardTextView.h"
#include "MinesweeperBoard.h"
#include "iostream"
using namespace  std;

class MSTextController {
MinesesweeperBoard &BoardGame;
MSBoardTextView &BoardView;
int height;
int width;
public:
     MSTextController(MSBoardTextView &view, MinesesweeperBoard &plansza);
    void play();
    void endGame();
};


#endif //SAPER2_MSTEXTCONTROLLER_H
