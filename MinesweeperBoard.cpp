//
// Created by x240 on 18.03.2022.
//

#include <iostream>
#include "MinesweeperBoard.h"
#include <cstdlib>
#include "cmath"
using namespace std;


MinesesweeperBoard::MinesesweeperBoard( GameMode mode){
//ustawić szer i wyskosc;


    if (mode==DEBUG) {
        width = 20;
        height = 20;
        Mines=100;
    }
    if (mode==EASY){
        width=4;
        height=4;
        Mines=ceil(width*height*0.1);

    }
    if (mode==NORMAL){
        width=6;
        height=6;
        Mines=ceil(width*height*0.2);
    }
    if (mode==HARD) {
         width = 9;
         height = 9;
        Mines=ceil(width*height*0.3);
    }
    //stan wszystkich pol na nie ma miny, nie ma flagi, jest odkryte
    //musimy wyczyścić plansza
    clear_board();
   // isPlayable()const;
   /* board[1][0].hasMine = true;
    board[0][2].hasMine = true;
    board[0][0].isRevealed = true;
    board[1][2].isRevealed = true;
    board[0][2].hasFlag = true;
    board[2][3].isRevealed = true;
    //ustawiamy kilka min*/
    setMines();


    state=RUNNING;


}

void MinesesweeperBoard::setMines(){
    int Miny=Mines;

    for(int row=rand()% width;row < width;row++){
        for (int col = rand()% height; col < height ; col++) {
            if(!board[col][row].hasMine && Miny!=0 && isPlayable(col, row)){
                board[col][row].hasMine=true;
                Miny--;
            }
            else{

            }
        }

    }
    }


   void MinesesweeperBoard::revealField(int col, int row){
    if (!board[col][row].isRevealed and !board[col][row].hasMine) {
        board[col][row].isRevealed = true;
       state = RUNNING;

    }
     if(!board[col][row].isRevealed and board[col][row].hasMine){
        board[col][row].isRevealed=true;
         state= FINISHED_LOSS;
    }
if(victory()== true){
    state=FINISHED_WIN;
}

}
bool MinesesweeperBoard::has_mine(int col, int row) const {
    if (board[col][row].hasMine){
        return true;
    }
    else return false;
}

/*bool MinesesweeperBoard::isPlayable() const {
    for (int row = 0; row < width; row++) {
        for (int col = 0; col < height; col++) {
            if ( row>= width or col >= height or col < 0 or row < 0) {
                return false;
            }
            return true;
        }
    }
}
 */
/*void MinesesweeperBoard::sprawdzenie(int row, int col){

}
*/
bool MinesesweeperBoard::victory() {
    int pola = getBoardWidth()*getBoardHeight();
    for (int col = 0; col < width; col++) {
        for (int row = 0; row < height; row++) {

            if(board[col][row].isRevealed && !board[col][row].hasMine)
                pola--;
        }

    }
    if(pola==getMineCount()){return true;}
    else
        return false;
}
bool MinesesweeperBoard::isPlayable(int col, int row)const{

    if(col >= width || row >= height||row<0||col<0)
        return false;
    return true;
}
int MinesesweeperBoard::countMines(int col, int row)const {

    //for (int row =0; row<=width; row++){
    // for(int col=0; col<=height; col++){
           int licznik=0;
           for(int kolumna = col -1; kolumna <= col + 1; kolumna++)
               for(int rzad = row - 1; rzad <= row + 1; rzad++)
               {
                   if(isPlayable(kolumna, rzad) and board[kolumna][rzad].hasMine)
                       licznik++;
               }
           return licznik-int(board[col][row].hasMine);
       }

//}
bool MinesesweeperBoard::has_Flag(int col, int row) const{
    if (board[col][row].hasFlag)
        return true;
    else {
        if (col < height or row < width or board[col][row].isRevealed or !board[col][row].hasFlag)
            return false;
    }

}


void MinesesweeperBoard::clear_board(){
    //tutaj czyscimy plansze
    // do czego ma sluzyc linia 51? Nadal Pan nie potrafi odniesc sie do konkretnego pola w strukturze. Okej modyfikacji wymagaw  linia 56. i tylko ona ;). Zdecydowanie nie w tym miejscu tylko przy jej koncu
    //Do elemntu tablicy 2 wymiarowej dobieramy sie przez tablica[numer 1][numer 2]?
    // studenci[idx].imie

    for(int row=0;row<width;row++){
        for (int col=0;col<height; col++ ){
            board[col][row].hasMine=false;
            board[col][row].hasFlag=false;
            board[col][row].isRevealed=false;

        }
    }
}

void MinesesweeperBoard::toggleFlag(int col, int row) {
    if(!board[col][row].hasFlag){
        board[col][row].hasFlag=true;
    }

    else if(board[col][row].hasFlag or !isPlayable(col,row) or state == FINISHED_LOSS or state ==FINISHED_WIN ){

    }

}

void MinesesweeperBoard::debug_display()const
{
    for(int col=0;col<width; col++ ){
        for (int row=0;row<height;row++){
            cout<<"[";
            if(board[col][row].hasMine){
                cout<<"M";

            }
            else
                cout<<".";
            if(board[col][row].isRevealed){
                cout<<"o";
            cout<<countMines(col,row);
            }
            else
                cout<<".";
            if(board[col][row].hasFlag)
                cout<<"f";
            else
                cout<<".";


            cout<<"]";
        }
        cout<<endl;
    }

}
GameState MinesesweeperBoard::getGameState() {
    return state;

}

int MinesesweeperBoard::getBoardWidth()const
{
    return width;
}
int MinesesweeperBoard::getBoardHeight()const
{
    return height;
}
int MinesesweeperBoard::getMineCount() const {
    return Mines;
}
bool MinesesweeperBoard::isRevealed(int col, int row) const{
    if (board[col][row].isRevealed){
        return true;
    }
    else{
            return false;
    }
}
char MinesesweeperBoard::getFieldInfo(int col, int row) const{
// convenience function - returns useful information about field in one function call
    // if row or col is outside board                         - return '#' character
    // if the field is not revealed and has a flag            - return 'F' character
    // if the field is not revealed and does not have a flag  - return '_' (underscore) character
    // if the field is revealed and has mine                  - return 'x' character
    // if the field is revealed and has 0 mines around        - return ' ' (space) character
    // if the field is revealed and has some mines around     - return '1' ... '8' (number of mines as a digit)


           if(!isPlayable(col,row)) {
               return '#';
           }
               if (!board[col][row].isRevealed && board[col][row].hasFlag) {
                   return 'F';
               }
               if (!board[col][row].hasFlag && !board[col][row].isRevealed) {
                   return '_';
               }
               if (board[col][row].hasMine && board[col][row].isRevealed) {
                   return 'x';
               }
               if (board[col][row].isRevealed && countMines(col, row) == 0) {

                   return ' ';
               }
               if (board[col][row].isRevealed && countMines(col, row) != 0) {
                   return char (countMines(col, row)+'0');
               }

           }
