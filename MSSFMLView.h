//
// Created by gracz on 2022-04-21.
//

#ifndef SAPERSFML_MSSFMLVIEW_H
#define SAPERSFML_MSSFMLVIEW_H

#include <SFML/Graphics/RenderWindow.hpp>
#include "MinesweeperBoard.h"

class MSSFMLView
{
    MinesesweeperBoard & board;

public:
    explicit MSSFMLView(MinesesweeperBoard & b);

    void draw (sf::RenderWindow & win); //plansza
    void drawPole (sf::RenderWindow &win );
};


#endif //SAPERSFML_MSSFMLVIEW_H
