//
// Created by gracz on 2022-04-21.
//
#include <SFML/Graphics/CircleShape.hpp>
#include <SFML/Graphics/RectangleShape.hpp>
#include <SFML/Graphics/Font.hpp>
#include <SFML/Graphics/Text.hpp>
#include <SFML/Graphics/Sprite.hpp>
#include "MinesweeperBoard.h"
#include "MSSFMLView.h"
MSSFMLView::MSSFMLView(MinesesweeperBoard & b) : board(b) {}

void MSSFMLView::draw (sf::RenderWindow & win)
{ sf::RectangleShape pole ;
sf::Font font;
sf::Texture bomba;


//bomba.loadFromFile("C://Users/gracz/CLionProjects/SaperSFML/Textures/bomba.png");
    bomba.loadFromFile('bomba.png");
//font.loadFromFile("C://Users/gracz/CLionProjects/SaperSFML/Fonts/font.ttf");
    font.loadFromFile("font.ttf");
    sf::Text text1;
    sf::Sprite bomba1{bomba};
    sf::Sprite bomba2{bomba};
    text1.setFont(font);
    pole.setSize ( sf::Vector2f(40, 40) ) ;
   bomba.create(40,40);
   bomba1.setTexture(bomba);

   bomba2.setPosition(100,100);
   win.draw(bomba2);
    // tu robimy rysowanie planszy na podstawie zawartości "board"
    for (int col = 0; col < board.getBoardHeight(); col++) {
        for (int row = 0; row < board.getBoardWidth(); row++) {
            pole.setPosition(100+row*50, 50+col*50);
            bomba1.setPosition(100+row*50, 50+col*50);
            pole.setOutlineColor(sf::Color{255,0,0});
            pole.setOutlineThickness(2.f);
            text1.setPosition(110+row*50,53+col*50);
            if(board.getFieldInfo(col,row)=='_'){
            pole.setFillColor ( sf::Color{220, 220, 220});

            win.draw(pole);
            }
            if(board.getFieldInfo(col,row)=='1'){
                pole.setFillColor ( sf::Color::Blue );
                text1.setString("1");
                win.draw(pole);
                win.draw(text1);
                }
            if(board.getFieldInfo(col,row)=='2'){
                pole.setFillColor ( sf::Color::Blue );
                text1.setString("2");
                win.draw(pole);
                win.draw(text1);
            }
            if(board.getFieldInfo(col,row)=='3'){
                pole.setFillColor ( sf::Color::Blue );
                text1.setString("3");
                win.draw(pole);
                win.draw(text1);
            }
            if(board.getFieldInfo(col,row)=='4'){
                pole.setFillColor ( sf::Color::Blue );
                text1.setString("4");
                win.draw(pole);
                win.draw(text1);
            }
            if(board.getFieldInfo(col,row)=='5'){
                pole.setFillColor ( sf::Color::Blue );
                text1.setString("5");
                win.draw(pole);
                win.draw(text1);
            }
            if(board.getFieldInfo(col,row)=='6'){
                pole.setFillColor ( sf::Color::Blue );
                text1.setString("6");
                win.draw(pole);
                win.draw(text1);
            }
            if(board.getFieldInfo(col,row)=='7'){
                pole.setFillColor ( sf::Color::Blue );
                text1.setString("7");
                win.draw(pole);
                win.draw(text1);
            }
            if(board.getFieldInfo(col,row)=='8'){
                pole.setFillColor ( sf::Color::Blue );
                text1.setString("8");
                win.draw(pole);
                win.draw(text1);
            }
                if(board.getFieldInfo(col,row)=='x'){
                pole.setFillColor ( sf::Color::Cyan );
                    bomba1.setPosition(300+row*50, 200+col*50);
                win.draw(pole);
                win.draw(bomba1);
            }
            if(board.getFieldInfo(col,row)=='F'){
                pole.setFillColor ( sf::Color::Yellow );

                win.draw(pole);
            }

            if(board.getFieldInfo(col,row)==' ' ){
                pole.setFillColor ( sf::Color::Green );

                win.draw(pole);
            }

        }
    }

}

void MSSFMLView::drawPole(sf::RenderWindow &win) {
sf::RectangleShape poleZakryte;
poleZakryte.setSize(sf::Vector2f(35,35));
poleZakryte.setFillColor(sf::Color::Blue);

}
