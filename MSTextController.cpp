//
// Created by gracz on 2022-04-07.
//


#include "MinesweeperBoardTextView.h"
#include "MinesweeperBoard.h"
#include "iostream"
#include "MSTextController.h"

using namespace  std;


MSTextController::MSTextController(MSBoardTextView &plansza, MinesesweeperBoard &boardGame): BoardView(plansza), BoardGame(boardGame) {
//    int height=BoardGame.getBoardHeight();
//    int width=BoardGame.getBoardWidth();
}
void MSTextController::play() {


    int s,row,col;
    while(BoardGame.getGameState()==RUNNING){
        BoardView.display();
        //BoardGame.debug_display();
        cout<<"Wybierz 1 zeby odslonic pole."<<endl;
        cout<<"Wybierz 2 zeby dac flage na  pole."<<endl;
        cin>>s;
        switch (s){
            case 1:
                cin>>col;
                cin>>row;
                BoardGame.revealField(col,row);
                break;
            case 2:
                cin>>col;
                cin>>row;
                BoardGame.toggleFlag(col,row);
                break;
        }
    }
    endGame();
}
void MSTextController::endGame(){
    if(BoardGame.getGameState()==FINISHED_LOSS)
        cout<<"Bum. U dead boi"<<endl;
    if(BoardGame.getGameState()==FINISHED_WIN)
        cout<<"Urodzony z ciebie saper"<<endl;
}