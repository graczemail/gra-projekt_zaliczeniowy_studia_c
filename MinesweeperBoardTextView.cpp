//
// Created by gracz on 2022-04-05.
//

#include "MinesweeperBoardTextView.h"
#include "MinesweeperBoard.h"
#include "iostream"
using namespace  std;

MSBoardTextView::MSBoardTextView(MinesesweeperBoard &board):BoardDisplay(board) {
//    int height=BoardDisplay.getBoardHeight();
//    int width=BoardDisplay.getBoardWidth();
}
void MSBoardTextView::wincon() {
    GameState state=BoardDisplay.getGameState();
    int height = BoardDisplay.getBoardHeight();
    int width = BoardDisplay.getBoardWidth();
    int liczbaPol = height * width;
    int polaRev=0;
    int polaFlag=0;

        for (int col = 0; col <BoardDisplay.getBoardHeight(); col++) {
            for (int row = 0; row < BoardDisplay.getBoardWidth(); row++) {
                if (BoardDisplay.has_mine(col, row) == 1 && BoardDisplay.has_Flag(col, row) == 1 &&
                    !BoardDisplay.isRevealed(col, row)) {
                    polaFlag++;
                }
                if (!BoardDisplay.has_mine(col, row) && BoardDisplay.isRevealed(col, row)) {
                    polaRev++;

                }
            }
            if(liczbaPol==polaRev+polaFlag &&BoardDisplay.getGameState()!=FINISHED_LOSS)
                state=FINISHED_WIN;

        }
    }


void MSBoardTextView::display() {
GameState state=BoardDisplay.getGameState();
    int height=BoardDisplay.getBoardHeight();
    int width=BoardDisplay.getBoardWidth();

    for (int col = 0; col < height; col++) {
        for (int row = 0; row < width; row++) {
            cout << "[" << BoardDisplay.getFieldInfo(col, row) << "]";
        }
        cout<<endl;
    }
}