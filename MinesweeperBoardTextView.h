//
// Created by gracz on 2022-04-05.
//

#ifndef SAPER2_MINESWEEPERBOARDTEXTVIEW_H
#define SAPER2_MINESWEEPERBOARDTEXTVIEW_H

#include "MinesweeperBoard.h"


class MSBoardTextView{
    MinesesweeperBoard &BoardDisplay;
//    int height;
//    int width;
public:
    explicit MSBoardTextView(MinesesweeperBoard &plansza);
    void display();
    void FieldInfo(MinesesweeperBoard &getFieldInfo);

    void wincon();
};


#endif //SAPER2_MINESWEEPERBOARDTEXTVIEW_H
